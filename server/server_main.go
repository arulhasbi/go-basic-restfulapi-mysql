// GOLANG tutorial simple restful api with mysql
// authored by Arul

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type employee struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Location string `json:"location"`
}

type account struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

var employees []employee
var signingKey = []byte("savethiskeyinyoursystem")

func main() {
	handleRequestDB()
}

func openDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:qwertyawsd@/dbexample")
	return db, err
}

func handleRequestDB() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePageDB)
	myRouter.HandleFunc("/employees", getAllEmployeeDB)
	myRouter.HandleFunc("/employee", createEmployeeDB).Methods("POST")
	myRouter.HandleFunc("/employee", updateEmployeeDB).Methods("PUT")
	myRouter.HandleFunc("/employee/{id}", deleteEmployeeDB).Methods("DELETE")
	myRouter.HandleFunc("/employee/{id}", getSingleEmployeeDB)

	http.Handle("/", myRouter)

	log.Fatal(http.ListenAndServe(":1996", nil))
}
func homePageDB(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "welcome to restful api with golang v2")
}

// get employees
func getAllEmployeeDB(w http.ResponseWriter, r *http.Request) {
	employees = []employee{}
	db, err := openDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	qryResult, qryErr := db.Query("SELECT id, name, location FROM employee")
	if qryErr != nil {
		log.Println(qryErr.Error())
	}
	defer qryResult.Close()

	for qryResult.Next() {
		var emp employee
		qryErr = qryResult.Scan(&emp.ID, &emp.Name, &emp.Location)
		if qryErr != nil {
			log.Println(qryErr.Error())
		}
		log.Println(emp.ID, emp.Name, emp.Location)
		employees = append(employees, emp)
	}
	json.NewEncoder(w).Encode(employees)
	fmt.Println("endpoint hit: getAllEMployeeDB")
}

// get single employee by id
func getSingleEmployeeDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, idErr := strconv.Atoi(vars["id"])
	if idErr != nil {
		panic(idErr.Error())
	}

	db, err := openDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	qryResult, qryErr := db.Query("SELECT id, name, location FROM employee where id = ?", id)

	defer qryResult.Close()

	var emp employee

	for qryResult.Next() {
		qryErr = qryResult.Scan(&emp.ID, &emp.Name, &emp.Location)
		if qryErr != nil {
			panic(qryErr.Error())
		}
		log.Println(emp.ID, emp.Name, emp.Location)
	}
	json.NewEncoder(w).Encode(emp)
	fmt.Println("endpoint hit: getSingleEmployeeDB")
}

func deleteEmployeeDB(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, idErr := strconv.Atoi(vars["id"])
	if idErr != nil {
		panic(idErr.Error())
	}

	db, err := openDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	qryResult, qryErr := db.Query("DELETE FROM employee where id = ?", id)

	defer qryResult.Close()

	var emp employee

	for qryResult.Next() {
		qryErr = qryResult.Scan(&emp.ID, &emp.Name, &emp.Location)
		if qryErr != nil {
			panic(err.Error())
		}
		log.Println("Succesfully deleted this record:", emp.ID, emp.Name, emp.Location)
	}

	fmt.Println("endpoint hit: deleteEmployeeDB")
}

func createEmployeeDB(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	var emp employee

	json.Unmarshal(reqBody, &emp)

	db, err := openDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	qryResult, qryErr := db.Query("INSERT INTO employee (name, location) values (?,?)", emp.Name, emp.Location)
	defer qryResult.Close()
	if qryErr != nil {
		panic(qryErr.Error())
	}

	fmt.Println("endpoint hit: createEmployeeDB")
}

func updateEmployeeDB(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	var emp employee

	json.Unmarshal(reqBody, &emp)

	// vars := mux.Vars(r)
	id, idErr := strconv.Atoi(emp.ID)

	if idErr != nil {
		panic(idErr.Error())
	}

	db, err := openDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	qryResult, qryErr := db.Query("UPDATE employee SET name = ?, location = ? where id = ?", emp.Name, emp.Location, id)
	defer qryResult.Close()
	if qryErr != nil {
		panic(qryErr.Error())
	}
	fmt.Println("endpoint hit: updateEmployeeDB")
}

//jwt
func isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Token"] != nil {
			token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return signingKey, nil
			})

			if err != nil {
				fmt.Fprintf(w, err.Error())
			}

			if token.Valid {
				endpoint(w, r)
			}
		} else {
			fmt.Fprintf(w, "Not Authorized")
		}
	})
}

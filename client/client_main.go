package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

var signingKey = []byte("savethiskeyinyoursystem")

func main() {
	handleRequest()
}

func generateJWT() (string, error) {

	toke := jwt.New(jwt.SigningMethodHS256)

	claims := toke.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["user"] = "Arul Hasbi"
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	log.Println(claims["exp"])

	tokenString, err := toke.SignedString(signingKey)

	if err != nil {
		// fmt.Errorf("Something went wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

func homePageMain(w http.ResponseWriter, r *http.Request) {
	validToken, err := generateJWT()

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	fmt.Fprintf(w, validToken)

	// this is how to quick test
	// client := &http.Client{}
	// req, _ := http.NewRequest("GET", "http://localhost:1996/", nil)
	// req.Header.Set("Token", validToken)
	// res, err := client.Do(req)
	// if err != nil {
	// 	fmt.Fprintf(w, "Error: %s", err.Error())
	// }

	// body, err := ioutil.ReadAll(res.Body)

	// if err != nil {
	// 	fmt.Fprintf(w, err.Error())
	// }

	// fmt.Fprintf(w, string(body))
}

func handleRequest() {
	http.HandleFunc("/", homePageMain)
	log.Fatal(http.ListenAndServe(":1000", nil))
}

package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

var signingKey = []byte("savethiskeyinyoursystem")

type account struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type employee struct {
	Name     string `json:"name"`
	Location string `json:"location"`
}

func main() {
	r := setupRouter()
	r.Run("localhost:1996")
}

func openDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:qwertyawsd@/dbexample")
	return db, err
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/account", createAccount)
	r.POST("/login", loginAccount)
	r.GET("/employees", auth, getEmployees)
	return r
}

// gin framework
func createAccount(c *gin.Context) {
	var acc account
	c.BindJSON(&acc)

	db, err := openDB()
	if err != nil {
		log.Println(err.Error())
	}
	defer db.Close()

	queryResult, queryErr := db.Query("INSERT INTO account (name, password) values (?,?)", acc.Name, acc.Password)
	if queryErr != nil {
		log.Println(queryErr)
	}
	defer queryResult.Close()

	c.JSON(200, gin.H{
		"status":  "200",
		"message": "succeed create an account",
	})
}

func loginAccount(c *gin.Context) {
	var loggedAcc account
	c.BindJSON(&loggedAcc)

	db, err := openDB()
	if err != nil {
		log.Println(err.Error())
	}
	defer db.Close()

	var getAcc account
	queryResult, queryErr := db.Query("SELECT name FROM account where password = ?", loggedAcc.Password)
	if queryErr != nil {
		log.Println(queryErr)
	}
	for queryResult.Next() {
		queryErr = queryResult.Scan(&getAcc.Name)
		if queryErr != nil {
			log.Println(queryErr.Error())
		}
		log.Println("Succeed login with this account: ", getAcc.Name)
	}
	defer queryResult.Close()

	sign := jwt.New(jwt.SigningMethodHS256)

	claims := sign.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(time.Second * 15).Unix()

	token, err := sign.SignedString(signingKey)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		c.Abort()
	}

	c.JSON(200, gin.H{
		"status":     "200",
		"message":    "succeed logged in with correct account",
		"validToken": token,
	})
}

func getEmployees(c *gin.Context) {
	db, err := openDB()
	if err != nil {
		log.Println(err.Error())
	}
	defer db.Close()

	queryResult, queryErr := db.Query("SELECT name, location FROM employee")
	if queryErr != nil {
		log.Println(queryErr.Error())
	}
	defer queryResult.Close()

	empCollection := []employee{}
	var emp employee

	for queryResult.Next() {
		queryErr = queryResult.Scan(&emp.Name, &emp.Location)
		if queryErr != nil {
			log.Println(queryErr.Error())
		}
		empCollection = append(empCollection, emp)
	}

	c.JSON(200, gin.H{
		"status": "200",
		"data":   empCollection,
	})
}

func auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")

	toke, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return signingKey, nil
	})

	if toke != nil && err == nil {
		fmt.Println("token verified")
	} else {
		result := gin.H{
			"message": "Not Authorized",
			"error":   err.Error(),
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}
